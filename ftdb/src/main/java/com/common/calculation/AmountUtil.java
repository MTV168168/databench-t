/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.common.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

/**
 * 金额计算工具类
 * 
 *
 */
public class AmountUtil {
	/**
	 * 随机获取金额
	 * @param min
	 * @param max
	 * @return
	 */
	public static BigDecimal getRandomMoney(final double min, final double max){
		double result = min + ((max - min) * new Random().nextDouble());
        BigDecimal bg = new BigDecimal(result).setScale(2, RoundingMode.UP);
		return bg;
	}
	/**
	 * 保留2位小数
	 * @param value
	 * @return
	 */
	public static String getStringDouble(String value) {
		return value;
	}
	
}
